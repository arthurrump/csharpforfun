<div align="center">
<img src="https://gitlab.com/arthurrump/csharpforfun/raw/master/logo/csharpforfunbot.png" alt="C# for fun bot" height="150em" />
<h1>C# for fun</h1>
<a href="https://twitter.com/csharpforfun"><img src="https://img.shields.io/badge/twitter-%40csharpforfun-1da1f2.svg?style=flat" alt="Twitter: @csharpforfun" /></a>
</div>

Welcome to the repo for C# for fun! To learn more about the reasons for this project, check out [this blog post](https://www.arthurrump.com/2017/07/23/thoughts-on-getting-started-with-net/).

The goal is to create a fun and easy tutorial for complete beginners to get started with C# and .NET Core very quickly. The single most important thing for this to succeed are good examples, that guide you through the tutorials. I chose for bots, because they are very easy to start with, and work very well across platforms. You could even say the standard Hello world program is a bot, and that's exactly what I did.

But, I'm also convinced that the best way to really learn something, is to understand what's going on behind the scenes, maybe even with a bit of history on how it worked before. That's why there's the 'deep dives' section, which contains articles that go deep into detail on a topic.