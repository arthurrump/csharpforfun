---
title: 1. Getting started on Windows
uid: tutorial-getting-started-on-windows
---

# 1. Getting started on Windows
Windows is home for C# and has been since its creation. The most popular program for writing C# on Windows has been, is, and probably will be, Visual Studio. However, Visual Studio is a big program, it can take quite long to install and it has many, many features we won't use in this series. It's also available on Windows only, and since this is a cross-platform series, that would mean differences between operating systems for every single tutorial. Luckily, Visual Studio has a little brother called Visual Studio Code, which together with the dotnet command line application is fast(er) to install and works everywhere. So let's get started.

## Install the tools
First, we'll download the .NET Core SDK, which is a package of all the tools you need to turn C# code into a running program. Follow [this link](https://www.microsoft.com/net/download/core) and download the Windows installer for your processor type.

> [!TIP]
> **Not sure what version you need?**
> Right-click on the Start-menu and choose System. This will open the About section in Settings and display information about your computer. If System type says you have a 64-bit operating system, you'll want to get the x64 version of the .NET Core SDK, if it says 32-bit, you'll want the x86 version.

After downloading, you run the installer, read the license terms, check the box to agree with those and click the install button. Grant permission to make changes to your PC, if asked, and the .NET SDK will start installing.

![The .NET SDK installer](windows\dotnet-installer.png)

> [!NOTE]
> Want to know more about .NET, and some of its history? Read [what .NET is](xref:dd-what-is-dotnet).

While that's installing we can start downloading Visual Studio Code, by going to [this link](https://code.visualstudio.com/) and clicking the big green Download for Windows button. Close the .NET SDK installer when it's done, and start the Visual Studio Code installer. Follow the wizard through the installation and make sure that on the Select Additional Tasks screen you select at least the options to add "Open with Code" to the context menu and the option "Add to PATH (available after restart)".

![Visual Studio Code installer](windows\vscode-installer-options.png)

When the installation is done, uncheck the option to launch Visual Studio Code (we'll launch it when we need it), and click Finish.

## Create a new project
In this tutorial we're going to use the command line to execute the code we write, manage our project and of course create new projects. If you've never seen a command line window before, don't worry, we'll guide you through everything you need to know. On Windows, you can use the Command Prompt or PowerShell, it doesn't really matter which one you choose. To open a new command line window, right-click on the Start-menu and choose either Command Prompt or Windows PowerShell. Doing so will open a window like one of these:

![Windows PowerShell and Command Prompt windows](windows\powershell-cmd.png)

By default, the command line opens in your current user folder, as you can see by the path displayed in front of the `>`-sign. If you want to create your new projects in a different folder, in a csharpforfun folder for example, you can navigate to that folder using the `cd` command (for **c**hange **d**irectory). To navigate to a folder named csharpforfun inside your current user folder, type `cd csharpforfun` and press enter. Note that this only works if the folder already exists! To create a new folder you can use the `md` command (for **m**ake **d**irectory), for example `md csharpforfun`.

> [!TIP]
> You can open an explorer window next to your command line window, to see the changes you make.

Now let's create our first bot! Open a command line window and create a new folder for our project by typing `md helloworld` and pressing enter. Next, navigate to the new folder by typing `cd HelloWorld`, followed by enter. Create a new project with `dotnet new console`, which will create the files for a new console application, an application that you can use with the command line. To execute this application, execute the `dotnet run` command. This will print the text "Hello World!" on the command line. Congratulations, you just made your first bot!

> [!NOTE]
> Wondering how this works? Read [what happens when you run your code](xref:dd-what-happens-when-you-run-your-code).

## Take a look at the code
What we just did, isn't complete magic, of course. With `dotnet new console` we created two files: HelloWorld.csproj and Program.cs. HelloWorld.csproj is used to create an application with multiple files and contains some more information about our program, including its name etc. The Program.cs file is where our actual C# code lives, so let's take a look! Open the folder in Visual Studio Code with the `code .` command. When you open Program.cs by clicking on it in the overview of the current folder, Code will tell you that you should install the C# extension:

![The 'C#' extension is recommended for this file type.](windows\vscode-cs-extension-rec.png)

Click on the Show Recommendations button and install both the C# and the Mono Debug extensions. When they're installed, click one of the Reload buttons to reload Visual Studio Code and enable the extensions. The output window will open, telling you it's installing OmniSharp and the .NET Core Debugger. Once that's done, Code will ask you if you want to add some files to enable building and debugging your application in Code. Click Yes, and close the output window.

![Required assets to build and debug are missing from 'HelloWorld'. Add them?](windows\vscode-required-assets.png)

Now lets look at the code in Program.cs, it should look like this:

[!code-csharp[Program.cs](~/samples/HelloWorld/Program.cs)]

The only line we're going to look at now, is line 9. This is the line that tells your program to write the text "Hello World!" to the command line, which is called the Console in .NET programs. How should we look at this? The Console is a thing, which can do stuff. To tell the Console to do something, we call one of its *methods*. One of the things the Console can do is write a line and to do this you have to *call* the `WriteLine` method. The dot between Console and WriteLine means that WriteLine is a method of Console. To call a method you put an opening and closing parenthesis after it. But we also have to tell the Console what it should write and to do that we put an *argument* between these parentheses, in this case the text "Hello World!". The quotes are required to distinguish a piece of text from the rest of the code. Finally, we have to end every line that tells the computer to do something, every *statement*, with a semicolon (;).

To summarize, we first tell the computer the thing we want to do something, then we tell it what we want that thing to do, and then we give some information about how to do it between the parentheses. To tell the computer the command is done, we put a semicolon. Pretty simple, if you think about it.

> [!NOTE]
> Wondering where those bin and obj folders come from? Read [this deep dive](xref:dd-what-are-these-bin-and-obj-folders-for).

## Challenge
Now it's your turn! Why not write a bot that greets you specifically, instead of the entire world? So your assignment for this lesson is to create a bot that writes "Hello Joe!" (or whatever your name is) to the command line. You can either modify our HelloWorld bot, or practice the entire process and create a new one, it's up to you. You will find the solution in the next tutorial, see you there!

## Going on
In this tutorial we've focused specifically on using C# and .NET on Windows, but in the next ones I'll give you more general instructions that should work across Windows, Linux and macOS, because writing everything thrice for all platforms is just a lot of work. What do you need to know? In the upcoming tutorials I expect you to know how to open a command line window (I'll try to use that term, but *console* and *terminal* mean the exact same thing), either Windows PowerShell or Command Prompt; how to navigate to the folder with you current project, and how to execute a command in the command line. That's pretty reasonable, right?