---
title: 3. Talking to Twitter
uid: tutorial-03-talking-to-twitter
---

# 3. Talking to Twitter

There's one thing that's more awesome than software, and that's software that can talk to other software. In this tutorial we're going to create a bot that talks to Twitter. But first we have an echo bot to handle!

## Solution to the challenge
In the previous tutorial you were asked to create a bot that just repeats after you. Here's a solution:

[!code-csharp[EchoBot](~/samples/EchoBot/Program.cs)]

First the program asks the `Console` to read a line. The console waits for you to press enter, and returns the text you typed. That text is than saved into a variable called `input`. Then we tell the console to write that text back to the command line.

That's just one possible solution, of course. If you found another way, please share it in the comments. We love to see what you come up with!

## Building on
In this tutorial we'll build a bot that asks the user for a Twitter-handle and then displays the latest tweet of that user. To enable our bot to talk to Twitter, we'll first have to tell Twitter about our bot. To do that, you go to [Twitter Application Management](https://apps.twitter.com/) and sign in with your Twitter account. Click on the Create New App button and give your bot a name and a description. If you don't have a website, just fill in http://example.com. Check the box to agree with the Development Agreement and click on Create your Twitter application. Keep the webpage open, we'll come back to it later.

Now let's create our application. Use the command `dotnet new console -o TwitterBot` to create a new application in a new TwitterBot folder and then navigate your command line to that folder. To talk to Twitter we're going to use a *library* called [SimpleTwitter](https://www.nuget.org/packages/SimpleTwitter). A library is a little package with classes and methods, so you don't have to write that code yourself. We've previously used the Console class, which is a part of .NET Core, so it's just available to us. If we want to use a class that's not in .NET Core, we'll either have to create it ourselves, or find a library that already has it. Later in this tutorial we'll use a Twitter class, which is not available in .NET Core, but only in the SimpleTwitter library. To use that library in our application we use the command `dotnet add package SimpleTwitter`. This will modify the .csproj file to link to the SimpleTwitter package and download it.

> [!NOTE]
> Want to know where that package comes from? Read [where packages live](xref:dd-where-do-packages-live).

Open the project in VS Code and take a look at Program.cs. We now this writes "Hello World!" to the console, but we still haven't talked about the first couple of lines. Here are the first lines of HelloWorld again:

[!code-csharp[HelloWorld using and namespace](~/samples/HelloWorld/Program.cs#L1-L5)]

The class `Program` is inside something called a *namespace*, which in this case is `HelloWorld`. A namespace groups classes together, like you group files together in folders. There's also a namespace called `System`, which is part of .NET Core. This namespace contains classes like `Console` and `String` which we both have used before. The reason we could use these classes in our code, is the first line: `using System;`. With this line we tell the computer we want to have access to all the classes in the namespace called `System`. And now, back to our Twitter bot!

The classes we'll use to talk to Twitter, which we added with the SimpleTwitter package, are in a namespace called `SimpleTwitter`. To use the classes in this namespace we'll add a new line to tell the computer we want to use the classes in the `SimpleTwitter` namespace:

[!code-csharp[TwitterBot usings](~/samples/TwitterBot/Program.cs#L1-L2)]