---
title: 2. Starting conversation
uid: tutorial-02-starting-conversation
---

# 2. Starting conversation

Wouldn't it be great if you could talk to your bot, instead of the one-way conversation we've had until now? Well, that's what we're going to build, but first the solution to the previous challenge.

## Solution to the challenge
You were asked to create a bot that greets you specifically, instead of just the whole world. You could very easily modify the HelloWorld bot to acomplish this task:

[!code-csharp[HelloJoe](~/samples/HelloJoe/Program.cs?highlight=9)]

How does that work? Remember we told the `Console` to `WriteLine` the text "Hello World!" and now we want it to print "Hello Joe!", so we have to change the *argument*, the thing between parentheses, to be "Hello Joe!" instead of "Hello World!"! Yep, that's it.

## Building on
So now we have a bot that can greet one specific person. That really limits the audience that can use you bot, so we should probably ask the user what their name is, before we greet them. Before we do that, let's talk a bit about where we'll put that code.

Up until now we've just modified one single line of the default application, but let's take a look at the rest of the code. We'll let the `using` and `namespace` lines for what they are, but the `class` and `Main` lines are interesting right now. We've talked about how `Console` is a thing that you can tell to do stuff. A better way to say that is that `Console` is a class. A class usually represents a thing in the real (or virtual) world. With `class Program { }` we say we want to make a thing that we call `Program`. That's a pretty good description of what it is, but we could be more specific: we're building a bot that greets the user, so `GreetingBot` would be a better name. It's usually a good idea to use names that are specific to what the thing really is. Go ahead and replace `Program` with `GreetingBot`, then try running your program again by executing the `dotnet run` command in your favorite command line.

> [!WARNING]
> **Hey, where's the semicolon?** In the previous tutorial I told you every statement, every command you give the computer has to end with a semicolon. Notice that after both `class Program` and `static void Main(...)` there's no semicolon. That's because these lines aren't a command for the computer to execute, but they tell the computer, 'Hey, see those things between the curly braces? Those are part of the Program class.' It's not a statement, so there's no semicolon.

And it still works! That's because .NET doesn't care about what you name your classes, as long as your entire program (which may have multiple classes, we'll see that later) has one, and not more than one, method called `Main`. That's the method .NET will call when you tell it to run your program with `dotnet run`. Our `Main` method is defined by `static void Main(string[] args)`. What do all those words mean? We'll cover `static` later, it wouldn't make any sense right now; `void` means that the method doesn't give back a result. If you had a method called `Multiply` which mulitplies to numbers, you'd want that method to *return* a number, but `Main` doesn't really have such a result to give back, so we have to put `void` there. Then `Main` is the name of the method, as you might've guessed, and now there are these parentheses again. Remember we used these parentheses when calling a method? We use them when we define the method too! Between these parentheses is a *parameter* with a type of `string[]` and a name of `args`. This is similar to how `WriteLine` has a parameter of type `string`, to which we passed the *argument* "Hello, World!", but in this case .NET handles the argument.

Types are used to tell the kind of information you're working with. The type `string` means a piece of text, which you have to put between double quotes (`"`). The type `string[]` means an *array* of strings, which means a list of pieces of text. You can actually make a list of any type, just by putting `[]` after it! We'll see this and many other types later on.

> [!NOTE]
> Want to learn more about types? Check out [this deep dive](xref:dd-what-are-types).

So the `Main` method is what's executed by .NET, wich means that if we want our code to be executed we'll have to put it inside that method. To do that we put the code between the curly braces (`{` and `}`) that come after the `static void Main(...)` line. Now that we know where to write our code, let's write it!

First, we want to ask the user their name, so we want to put something like "Hi, what's your name?" on the screen. We'll tell the `Console` to `WriteLine` that text:

[!code-csharp[GreetingBot.Main](~/samples/GreetingBot/Program.cs?range=7-9,12)]

That's pretty straightforward, nothing different from what we've done until now. But now we want the user to be able to respond, and our program to pick up that response. We want the `Console` to read what the user typed in their command line. How about the `ReadLine` method? That sounds like it should work. `ReadLine` is a bit different from `WriteLine` in two ways: first `ReadLine` doesn't accept any arguments, and second `ReadLine` gives back the text it read, whereas `WriteLine` returns nothing. The first difference is easy, we just don't give the method any arguments when calling it:

```csharp
static void Main(string[] args)
{
    Console.WriteLine("Hi, what's your name?");
    Console.ReadLine();
}
```

But we still don't know what text the user typed. We'll have to store that text somehow. For that we can use something called a *variable*. A variable is like a little box in which you can put some information. We're gonna use the `var` keyword to create a variable named `name` in which we put the text we get from `ReadLine`:

[!code-csharp[GreetingBot.Main](~/samples/GreetingBot/Program.cs?range=7-10,12)]

We can now use `name` like any other piece of text, even add it together with any other piece text, just using the `+` sign. If we put the text "Joe" in `name`, `"Hello, " + name + "!"` will give the text "Hello, Joe!". We'll use that to greet our user by name:

[!code-csharp[GreetingBot.Main](~/samples/GreetingBot/Program.cs#L7-L12)]

You can now run your bot with `dotnet run`. The bot should ask you for your name, and if you reply and press enter, it will greet you personally. Awesome.

## Challenge
Now you'll have to think for yourself a bit: you're gonna build an echo bot. The bot will wait for you type something, and when you press enter it will repeat you. And don't forget to give the class a good name, `EchoBot` for example! The solution awaits you in the next tutorial, good luck!