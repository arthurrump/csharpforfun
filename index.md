---
title: C# for Fun (Preview)
---

# Welcome to C# for Fun
Choose a platform to get started:
- [Windows](/tutorial/01-getting-started/windows.html)
- [Mac](/tutorial/01-getting-started/mac.html)
- [Linux](/tutorial/01-getting-started/linux.html)