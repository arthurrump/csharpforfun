﻿using System;

namespace EchoBot
{
    class EchoBot
    {
        static void Main(string[] args)
        {
            var input = Console.ReadLine();
            Console.WriteLine(input);
        }
    }
}
