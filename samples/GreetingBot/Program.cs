﻿using System;

namespace GreetingBot
{
    class GreetingBot
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hi, what's your name?");
            var name = Console.ReadLine();
            Console.WriteLine("Hello, " + name + "!");
        }
    }
}
